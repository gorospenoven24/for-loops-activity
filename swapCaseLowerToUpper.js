
// Swap lower to upper and upper to lower
function swapCase(str){
    let newArray=[]
    let letters = str.split('')
    for (let letter of letters){
     
        if(letter === letter.toUpperCase()){
            newArray.push(letter.toLowerCase())
        }else{
            newArray.push(letter.toUpperCase())
        }
      
    }
    return newArray.join('')
}
let output = swapCase('NoVel')
console.log(output)

